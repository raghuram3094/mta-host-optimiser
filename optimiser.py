from contextlib import closing
import sqlite3
import json 
import jsonify
import MySQLdb

DB = 'localhost:3306/ipconfig'

class App:

  def main();
      initialise()
      host()



  def initialise():
      with app.app_context():
          db = get_db()
          with app.open_resource('sample.sql', mode='r') as f:
              db.cursor().executescript(f.read())
          db.commit()


  def get_db():
      db = getattr(g, '_database', None)
      if db is None:
          db = g._database = sqlite3.connect(DB)
      return db


  def query(query, args=(), one=False):
      cur = get_db().execute(query, args)
      rv = cur.fetchall()
      cur.close()
      return (rv[0] if rv else None) if one else rv


  def welcome():
      response = {'Hello': 'World'}
      return jsonify(response)


  def host():
      value = 1

      try:
          x = int(request.args.get('threshold'))
          if x > 0:
              value = x
      except Exception as e:
          pass


      cursor = DB.cursor()

      sql="SELECT Hostname FROM IP_TABLES where active=1 group by Hostname having count(active) <={0}"      
      query = query.format(sql)


      data = {
          "threshold": value,
          "hosts": []
      }
      for row in query(query):
          data['hosts'].append(row[0])

      final = jsonify(data)

      print final
 
 main(App)