create table IP_TABLES (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   IP VARCHAR(64) NOT NULL UNIQUE,
   HOSTNAME VARCHAR(128) NOT NULL,
   ACTIVE INTEGER NOT NULL,
   CHECK(active>=0 AND active <=1)
);


insert into IP_TABLES (IP, HOSTNAME, ACTIVE) values('127.0.0.1','mta-prod-1', 1);
insert into IP_TABLES (IP, HOSTNAME, ACTIVE) values('127.0.0.2','mta-prod-1', 0);
insert into IP_TABLES (IP, HOSTNAME, ACTIVE) values('127.0.0.3','mta-prod-2', 1);
insert into IP_TABLES (IP, HOSTNAME, ACTIVE) values('127.0.0.4','mta-prod-2', 1);
insert into IP_TABLES (IP, HOSTNAME, ACTIVE) values('127.0.0.5','mta-prod-2', 0);
insert into IP_TABLES (IP, HOSTNAME, ACTIVE) values('127.0.0.6','mta-prod-3', 0);
