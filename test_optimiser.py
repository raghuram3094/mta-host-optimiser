from App import App

import unittest
import os

class AppTest(unittest.TestCase):

    def db_test(self):
        tester = os.path.exists("sample.sql")
        self.assertTrue(tester)

    def host_test(self):
        tester = App.Test_client(self)
        response = tester.get('/host/', content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"hosts":["mta-prod-1","mta-prod-3"],"threshold":1}\n')

if __name__ == '__main__':
    AppTest.main()